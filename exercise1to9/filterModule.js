var fs = require('fs');
var pathModule = require('path');

module.exports = function (path, filteredBy, callback){
	fs.readdir(path, function countFiles(error, list){
		if (error) {
			return callback(error);
		};
		listFiltered = list.filter(function (file){
			return pathModule.extname(file) === "." + filteredBy;
		});
		callback(null, listFiltered);
	});
}