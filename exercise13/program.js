var http = require('http');
var url = require('url');
var port = Number(process.argv[2]);
var parseTimeURL = '/api/parsetime';
var unixtimeURL = '/api/unixtime';


function unixTime(time){
	return {
		unixtime: time.getTime()
	}
}

function parseTime(time){
	return {
		hour: time.getHours(),
		minute: time.getMinutes(),
		second: time.getSeconds()
	}
}



var server = http.createServer(function (request, response) {
	if (request.method != "GET") {
			return response.end('Not allowed Method.');
		}
	var timeResult;
	var urlObj = url.parse(request.url, true);
	var time = new Date(urlObj.query.iso);
	var pathNameStr = urlObj.pathname.toString();

	if (pathNameStr == parseTimeURL) {
		timeResult = parseTime(time);
	} else if (pathNameStr == unixtimeURL) {
		timeResult = unixTime(time);
	};

	if(timeResult){
		response.writeHead(200, {'content-type' : 'application/json'});
		response.end(JSON.stringify(timeResult));
	} 

})
server.listen(port);