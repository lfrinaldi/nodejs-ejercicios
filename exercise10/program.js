var net = require('net')
var port = Number(process.argv[2]);

function fillWithZero(data){
	return ((data < 10) ? '0' : '') + data;
}

function getStringDate(){
	var date = new Date();
	var dateStr = date.getFullYear() + '-' 
				+ fillWithZero(date.getMonth() + 1) + '-'
				+ fillWithZero(date.getDate()) + ' '
				+ fillWithZero(date.getHours()) + ':'
				+ fillWithZero(date.getMinutes());
	return dateStr;
}


var server = net.createServer(function (socket) {
  // socket handling logic
  socket.end(getStringDate() + '\n');
})
server.listen(port);